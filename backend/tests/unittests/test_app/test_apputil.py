import app.apputil as apputil
import app.constants as constants
import models.inmatemodel as inmatemodel
import common.util as util
import tests.unittests.testutil as testutil

from datetime import datetime

def test_normalizeUsPhoneNumber() :
	assert apputil.normalizeUsPhoneNumber('1234567890') == '+11234567890'
	assert apputil.normalizeUsPhoneNumber('(123)4567-890') == '+11234567890'

def test_getInmateNumberFromSender() :
	assert apputil.getInmateNumberFromSender('MILLER ALVIN E JR (04068095)') == '04068095'
	assert apputil.getInmateNumberFromSender('MILLER ALVIN E JR ( 04068095 )') == '04068095'

def test_extractPhoneNumber() :
	assert apputil.extractPhoneNumber('1234567890') == None
	assert apputil.extractPhoneNumber('#1234567890', False)['number'] == '1234567890'
	assert apputil.extractPhoneNumber('#1234567890', False)['index'] == 1

def test_splitTextAtWordBoundary() :
	assert apputil.splitTextAtWordBoundary('abc def gh ijkl', 8) == [ 'abc def', 'gh ijkl' ]
	assert apputil.splitTextAtWordBoundary('abc def gh ijkl', 9) == [ 'abc def', 'gh ijkl' ]
	assert apputil.splitTextAtWordBoundary('abc def gh ijkl', 10) == [ 'abc def gh', 'ijkl' ]
	assert apputil.splitTextAtWordBoundary('这是一个汉字 these are english words', 10) == [ '这是一个汉字', 'these are', 'english', 'words' ]
	assert apputil.splitTextAtWordBoundary('这是一个汉字 these are english words', 12) == [ '这是一个汉字 these', 'are english', 'words' ]

def test_getInmateNumberFromEmailAddress() :
	assert apputil.getInmateNumberFromEmailAddress('abc+12345678@bbb.com') == '12345678'
	assert apputil.getInmateNumberFromEmailAddress('abc+12-3456-78@bbb.com') == '12345678'
	assert apputil.getInmateNumberFromEmailAddress('abc+12-3456-789@bbb.com') == None

def test_computeBilling() :
	inmateModel = inmatemodel.InmateModel()

	def createInmate(expiryTimestamp, billingDay, trialExpiry = 0, credit = 100) :
		inmate = {}
		inmate[inmateModel.column_service_expiry] = util.calculateExpiryTime(expiryTimestamp, 0)
		inmate[inmateModel.column_billing_day] = billingDay
		inmate[inmateModel.column_trial_expiry] = trialExpiry
		inmate[inmateModel.column_credit] = apputil.dollarToCredit(credit)
		return inmate

	def checkResult(result, expiry, billingDay) :
		if result == None :
			assert False
			return False
		expiry = util.calculateExpiryTime(expiry, 0)
		if result[inmateModel.column_service_expiry] != expiry :
			print("===== test_computeBilling.checkResult: "
				+ str(datetime.fromtimestamp(result[inmateModel.column_service_expiry]))
				+ " != "
				+ str(datetime.fromtimestamp(expiry))
			)
			assert result[inmateModel.column_service_expiry] == expiry
			return False

		if result[inmateModel.column_billing_day] != billingDay :
			assert result[inmateModel.column_billing_day] == billingDay
			return False

		return True

	monthlyFee = apputil.dollarToCredit(20)

	assert apputil.computeBilling(createInmate(testutil.makeTimestamp(2019, 5, 8), 1), testutil.makeTimestamp(2019, 5, 1), monthlyFee) == None
	assert checkResult(
		apputil.computeBilling(
				createInmate(testutil.makeTimestamp(2019, 5, 8), 8),
				testutil.makeTimestamp(2019, 5, 8),
				monthlyFee
			),
		testutil.makeTimestamp(2019, 6, 8),
		8
	)
	assert checkResult(
		apputil.computeBilling(
				createInmate(testutil.makeTimestamp(2019, 5, 8), 8),
				testutil.makeTimestamp(2019, 5, 10),
				monthlyFee
			),
		testutil.makeTimestamp(2019, 6, 10),
		10
	)

	assert checkResult(
		apputil.computeBilling(
				createInmate(testutil.makeTimestamp(2019, 1, 31), 31),
				testutil.makeTimestamp(2019, 1, 31),
				monthlyFee
			),
		testutil.makeTimestamp(2019, 2, 28),
		31
	)
	assert checkResult(
		apputil.computeBilling(
				createInmate(testutil.makeTimestamp(2019, 2, 28), 31),
				testutil.makeTimestamp(2019, 2, 28),
				monthlyFee
			),
		testutil.makeTimestamp(2019, 3, 31),
		31
	)

def test_getInmatePaymentStatus() :
	def makeInmate(trialExpiry, serverExpiry, credit) :
		inmate = {}
		inmate[inmatemodel.InmateModel.column_trial_expiry] = trialExpiry
		inmate[inmatemodel.InmateModel.column_service_expiry] = serverExpiry
		inmate[inmatemodel.InmateModel.column_credit] = credit
		return inmate

	assert apputil.getInmatePaymentStatus(makeInmate(1000, 1000, 0), 500) == constants.InmatePaymentStatus.trial
	assert apputil.getInmatePaymentStatus(makeInmate(1000, 1000, 10), 500) == constants.InmatePaymentStatus.paid
	assert apputil.getInmatePaymentStatus(makeInmate(1000, 2000, 0), 500) == constants.InmatePaymentStatus.paid
	assert apputil.getInmatePaymentStatus(makeInmate(1000, 2000, 10), 500) == constants.InmatePaymentStatus.paid
	assert apputil.getInmatePaymentStatus(makeInmate(1000, 2000, 0), 3000) == constants.InmatePaymentStatus.unpaid
	assert apputil.getInmatePaymentStatus(None, 3000) == constants.InmatePaymentStatus.unpaid
