import common.util as util
import common.logger as logger

class AppSession :
	def __init__(self) :
		self._sessionFileName = 'session.json'
		self._session = {
			'running' : False,
			'quit' : False,
		}
		self._previousFileTime = 0

	def load(self) :
		self._previousFileTime = util.getFileTime(self._sessionFileName)
		session = util.decodeJson(util.readFileContent(self._sessionFileName))
		self._session['running'] = util.getDictValue(session, 'running', self._session['running'])
		self._session['quit'] = util.getDictValue(session, 'quit', self._session['quit'])

	def save(self) :
		util.writeFile(self._sessionFileName, util.encodeJson(self._session))

	def isRunning(self) :
		return self._session['running']

	def shouldQuit(self) :
		return self._session['quit']

	def setRunning(self, running = True) :
		self._session['running'] = running

	def setQuit(self, quit = True) :
		self._session['quit'] = quit

	def checkReload(self) :
		if self._previousFileTime != util.getFileTime(self._sessionFileName) :
			self.load()
			return True
		return False
