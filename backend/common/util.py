import codecs
import json
import time
import datetime
import binascii
import html
import threading
import traceback
import re
import os
import random
import base64

def getFileTime(fileName) :
	try :
		return int(os.path.getmtime(fileName))
	except :
		return 0

def writeFile(fileName, message) :
	file = codecs.open(fileName, "w", "utf-8")
	file.write(str(message))
	file.close()

def appendFile(fileName, message) :
	file = codecs.open(fileName, "a", "utf-8")
	file.write(str(message))
	file.close()

def writeDebugFile(fileName, message) :
	writeFile('data/temp/test_' + fileName, message)

def encodeJson(obj) :
	try :
		return json.dumps(obj)
	except Exception as e:
		print(e)
		return None
		
def decodeJson(content) :
	try :
		return json.loads(content)
	except :
		return None
		
def sleepForSeconds(seconds) :
	if seconds < 0 :
		seconds = 0
	time.sleep(seconds)

def sleepForMilliseconds(milliseconds) :
	if milliseconds < 0 :
		milliseconds = 0
	time.sleep(milliseconds * 0.001)
	
def getCurrentTimestamp() :
	return int(time.time())

def binToHex(s) :
	return s.hex()

def hexToBin(s) :
	return bytes.fromhex(s)

def isEmptyText(n) :
	return n == None or n == ''
	
def isEmptyList(n) :
	return n == None or len(n) == 0

def isString(s) :
	return isinstance(s, str)

def containsText(content, part) :
	return content.find(part) >= 0

def unescapeHtml(text) :
	return html.unescape(text)

def trim(text) :
	if text == None :
		return text
	return text.strip()
	
def quote(text) :
	return '"' + text + '"'
	
def joinListToString(list, delimiter = ',', callback = None) :
	result = ''
	for i in range(len(list)) :
		if i > 0 :
			result += delimiter
		item = list[i]
		if callback != None :
			item = callback(item)
		result += str(item)
	return result

def mergeDicts(a, b) :
	if a == None :
		a = {}
	if b == None :
		b = {}
	return { **a, ** b }

def getDictValue(dict, key, default = None) :
	if dict != None and key in dict :
		return dict[key]
	return default

def readFileContent(fileName) :
	try :
		with open(fileName) as file :
			return file.read()
	except :
		return ''

def getCurrentThreadId() :
	return threading.current_thread().ident

def splitEmailAddress(email) :
	parts = email.split('@')
	if len(parts) == 2 :
		return {
			'name' : parts[0],
			'domain' : parts[1],
		}
	if len(parts) == 1 :
		return {
			'name' : parts[0],
			'domain' : '',
		}
	return {
		'name' : '',
		'domain' : '',
	}

def emailAddressHasDomain(email, domain) :
	return splitEmailAddress(email)['domain'].lower() == domain.lower()

def printStackTrace() :
	traceback.print_stack()

def dateTimeToTimestamp(dt) :
	if dt == None :
		return 0
	return dt.timestamp()
	
def timestampToDateTime(timestamp, defaultValue = None) :
	if timestamp == 0 :
		return defaultValue
	return datetime.datetime.fromtimestamp(timestamp)

# [min, max)
def getRandomInt(min, max) :
	if min >= max :
		return max
	return random.randint(min, max - 1)

upperCharSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
lowerCharSet = 'abcdefghijklmnopqrstuvwxyz'
digitCharSet = '0123456789'

def getRandomString(length, charSet = lowerCharSet) :
	charSetLength = len(charSet)
	result = ''
	for _ in range(length) :
		result += charSet[getRandomInt(0, charSetLength)]
	return result

def getRandomMixedText(upperCount = 2, lowerCount = 8, digitCount = 2) :
	text = getRandomString(upperCount, upperCharSet)
	text += getRandomString(lowerCount, lowerCharSet)
	text = shuffleString(text)
	text += getRandomString(digitCount, digitCharSet)
	return text

def shuffleString(s) :
	return ''.join(random.sample(s, len(s)))

def daysToSeconds(days) :
	return days * 24 * 60 * 60

def minutesToSeconds(minutes) :
	return minutes * 60

def roundDownTimestampToMidnight(timestamp) :
	dt = datetime.datetime.fromtimestamp(timestamp)
	dt = datetime.datetime(dt.year, dt.month, dt.day, 0, 0, 0)
	return int(dt.timestamp())

def roundUpTimestampToMidnight(timestamp) :
	dt = datetime.datetime.fromtimestamp(timestamp)
	dt = datetime.datetime(dt.year, dt.month, dt.day, 0, 0, 0)
	return int(dt.timestamp() + daysToSeconds(1))

def calculateExpiryTime(currentTimestamp, expiryDays) :
	return roundUpTimestampToMidnight(currentTimestamp) + daysToSeconds(expiryDays) - 1

def getDayOfTimestamp(timestamp) :
	dt = datetime.datetime.fromtimestamp(timestamp)
	return dt.day

def base64Decode(text) :
	try :
		text = text.replace('\r','')
		text = text.replace('\n','')
		return base64.b64decode(text, validate = True).decode("utf-8")
	except :
		return None
