import models.basemodel as basemodel
import common.util as util

class ProcessedSmsModel(basemodel.BaseModel) :
	table = 'processed_sms'

	column_id = 'id'
	column_uid = 'uid'
	column_time = 'time'

	def __init__(self) :
		basemodel.BaseModel.__init__(self)
		
	def smsWasProcessed(self, uid) :
		query = '''
			select * from {tableName}
			where {column_uid} = %s
		'''.format(
			tableName = self.table,
			column_uid = self.column_uid,
		)
		with self.getDatabase().cursor() as cursor :
			cursor.execute(query, [
				uid
			])
			rows = cursor.fetchAll()
			return len(rows) > 0

	def processedSms(self, uid) :
		query = '''
			insert into {tableName}
			({column_uid}, {column_time})
			values (%s, %s)
		'''.format(
			tableName = self.table,
			column_uid = self.column_uid,
			column_time = self.column_time,
		)
		with self.getDatabase().transaction(), self.getDatabase().cursor() as cursor :
			cursor.execute(query, [
				uid,
				util.getCurrentTimestamp(),
			])

