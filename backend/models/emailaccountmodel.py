import models.basemodel as basemodel
import common.util as util

class EmailAccountModel(basemodel.BaseModel) :
	table = 'email_account'

	column_id = 'id'
	column_email = 'email'
	column_password = 'password'
	column_server = 'server'

	def __init__(self) :
		basemodel.BaseModel.__init__(self)

	def syncAccounts(self, accounts) :
		with self.getDatabase().transaction(), self.getDatabase().cursor() as cursor :
			for account in accounts :
				query = '''
					insert ignore into {tableName}
					({column_email}, {column_password}, {column_server})
					values (%s, %s, %s)
				'''.format(
					tableName = self.table,
					column_email = self.column_email,
					column_password = self.column_password,
					column_server = self.column_server,
				)
				cursor.execute(query, [
					account['address'],
					account['password'],
					util.getDictValue(account, 'server', ''),
				])

	def getAllAccounts(self) :
		query = '''
			select * from {tableName}
		'''.format(
			tableName = self.table,
		)
		with self.getDatabase().cursor() as cursor :
			cursor.execute(query)
			accounts = cursor.fetchAll()
			return accounts

	def getRandomAccount(self) :
		query = '''
			select * from {tableName}
		'''.format(
			tableName = self.table,
		)
		with self.getDatabase().cursor() as cursor :
			cursor.execute(query)
			accounts = cursor.fetchAll()
			if len(accounts) == 0 :
				return None
			return accounts[util.getRandomInt(0, len(accounts))]
