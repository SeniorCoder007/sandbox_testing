import models.basemodel as basemodel
import common.util as util
import common.database as database

class PhoneNumberModel(basemodel.BaseModel) :
	table = 'phone_number'

	column_id = 'id'
	column_inmate_number = 'inmate_number'
	column_phone_number = 'phone_number'

	def __init__(self) :
		basemodel.BaseModel.__init__(self)

	def usePhoneNumber(self, inmateNumber) :
		with self.getDatabase().transaction(), self.getDatabase().cursor() as cursor :
			query = '''
				select * from {tableName}
				where {column_inmate_number} = ''
			'''.format(
				tableName = self.table,
				column_inmate_number = self.column_inmate_number,
			)
			cursor.execute(query)
			rows = cursor.fetchAll()
			if len(rows) > 0 :
				row = rows[util.getRandomInt(0, len(rows))]
				phoneNumber = row[self.column_phone_number]
				query = '''
					update {tableName}
					set {column_inmate_number} = %s
					where {column_phone_number} = %s
				'''.format(
					tableName = self.table,
					column_inmate_number = self.column_inmate_number,
					column_phone_number = self.column_phone_number,
				)
				cursor.execute(query, [ inmateNumber, phoneNumber ])
				return phoneNumber
		return ''

	def recyclePhoneNumber(self, inmateNumber) :
		data = {}
		data[self.column_inmate_number] = ''
		where = {}
		where[self.column_inmate_number] = inmateNumber
		self.update(self.table, data, where)