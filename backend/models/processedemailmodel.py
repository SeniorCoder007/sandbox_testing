import models.basemodel as basemodel
import common.util as util

class ProcessedEmailModel(basemodel.BaseModel) :
	table = 'processed_email'

	column_id = 'id'
	column_email = 'email'
	column_folder = 'folder'
	column_uid = 'uid'
	column_time = 'time'

	def __init__(self) :
		basemodel.BaseModel.__init__(self)

	def getUnprocessedUidList(self, uidList, email, folder) :
		if len(uidList) == 0 :
			return []
		uidRange = util.joinListToString(uidList, ',', util.quote)
		query = '''
			select {column_uid} from {tableName}
			where {column_email} = %s and {column_folder} = %s and {column_uid} in ({uidRange})
		'''.format(
			tableName = self.table,
			column_uid = self.column_uid,
			column_email = self.column_email,
			column_folder = self.column_folder,
			uidRange = uidRange,
		)
		
		uidMap = {}
		for uid in uidList :
			uidMap[uid] = True

		with self.getDatabase().cursor() as cursor :
			cursor.execute(query, [
				email,
				folder
			])
			rows = cursor.fetchAll()
			for row in rows :
				uidMap[row[self.column_uid]] = False

		resultList = []
		for uid in uidMap :
			if uidMap[uid] :
				resultList.append(uid)
		return resultList

	def processedEmail(self, uid, email, folder) :
		query = '''
			insert into {tableName}
			({column_email}, {column_folder}, {column_uid}, {column_time})
			values (%s, %s, %s, %s)
		'''.format(
			tableName = self.table,
			column_email = self.column_email,
			column_folder = self.column_folder,
			column_uid = self.column_uid,
			column_time = self.column_time,
		)
		with self.getDatabase().transaction(), self.getDatabase().cursor() as cursor :
			cursor.execute(query, [
				email,
				folder,
				uid,
				util.getCurrentTimestamp(),
			])

