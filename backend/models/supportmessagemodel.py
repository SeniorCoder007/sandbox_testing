import models.basemodel as basemodel
import common.util as util
import common.logger as logger
import app.constants as constants

class SupportMessageModel(basemodel.BaseModel) :
	table = 'support_message'
	
	column_id = 'id'
	column_inmate_number = 'inmate_number'
	column_processed = 'processed'
	column_subject = 'subject'
	column_content = 'content'
	column_time = 'time'
	
	def __init__(self) :
		basemodel.BaseModel.__init__(self)
		
	def addMessage(self, inmateNumber, subject, content) :
		data = {}
		data[self.column_processed] = 0
		data[self.column_inmate_number] = inmateNumber
		data[self.column_subject] = subject
		data[self.column_content] = content
		data[self.column_time] = util.getCurrentTimestamp()
		self.insert(self.table, data)
