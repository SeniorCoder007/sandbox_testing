import models.basemodel as basemodel
import common.util as util
import common.logger as logger
import app.constants as constants

class InmateModel(basemodel.BaseModel) :
	table = 'inmate'
	
	column_id = 'id'
	column_email = 'email'
	column_inmate_number = 'inmate_number'
	column_phone_number = 'phone_number'
	column_credit = 'credit'
	column_create_time = 'create_time'
	column_trial_expiry = 'trial_expiry'
	column_billing_day = 'billing_day'
	column_service_expiry = 'service_expiry'
	column_corrlinks_password = 'corrlinks_password'
	column_corrlinks_first_name = 'corrlinks_first_name'
	column_corrlinks_last_name = 'corrlinks_last_name'
	column_corrlinks_session = 'corrlinks_session'

	def __init__(self) :
		basemodel.BaseModel.__init__(self)
		
	def getCorrLinksSession(self, email) :
		query = '''
			select {column_corrlinks_session} from {tableName}
			where {column_email} = '{email}'
		'''.format(
			tableName = self.table,
			column_corrlinks_session = self.column_corrlinks_session,
			column_email = self.column_email,
			email = email,
		)
		with self.getDatabase().cursor() as cursor :
			cursor.execute(query)
			row = cursor.fetchOne()
			if row != None :
				return row[self.column_corrlinks_session]
		return None

	def setCorrLinksSession(self, email, session) :
		query = '''
			update {tableName}
			set {column_corrlinks_session} = '{session}'
			where {column_email} = '{email}'
		'''.format(
			tableName = self.table,
			column_corrlinks_session = self.column_corrlinks_session,
			column_email = self.column_email,
			session = session,
			email = email,
		)
		with self.getDatabase().transaction(), self.getDatabase().cursor() as cursor :
			cursor.execute(query)

	def getInmatesByShard(self, count, offset, shardCount, shardIndex) :
		query = '''
			select * from {tableName}
			where {column_id} % {shardCount} = {shardIndex}
			limit {offset}, {count}
		'''.format(
			tableName = self.table,
			column_id = self.column_id,
			shardCount = shardCount,
			shardIndex = shardIndex,
			offset = offset,
			count = count,
		)
		with self.getDatabase().cursor() as cursor :
			cursor.execute(query)
			return cursor.fetchAll()
		return []

	def getInmateByInmateNumber(self, inmateNumber) :
		if inmateNumber == None :
			return None
		query = '''
			select * from {tableName}
			where {column_inmate_number} = %s
		'''.format(
			tableName = self.table,
			column_inmate_number = self.column_inmate_number,
		)
		with self.getDatabase().cursor() as cursor :
			cursor.execute(query, [ inmateNumber ])
			return cursor.fetchOne()
		return None

	def getInmateByPhoneNumber(self, phoneNumber) :
		if phoneNumber == None :
			return None
		query = '''
			select * from {tableName}
			where {column_phone_number} = %s
		'''.format(
			tableName = self.table,
			column_phone_number = self.column_phone_number,
		)
		with self.getDatabase().cursor() as cursor :
			cursor.execute(query, [ phoneNumber ])
			return cursor.fetchOne()
		return None

	def getInmateByEmail(self, email) :
		if email == None :
			return None
		query = '''
			select * from {tableName}
			where {column_email} = %s
		'''.format(
			tableName = self.table,
			column_email = self.column_email,
		)
		with self.getDatabase().cursor() as cursor :
			cursor.execute(query, [ email ])
			return cursor.fetchOne()
		return None

	def getAllInmates(self) :
		query = '''
			select * from {tableName}
		'''.format(
			tableName = self.table,
		)
		with self.getDatabase().cursor() as cursor :
			cursor.execute(query)
			return cursor.fetchAll()
		return None

	def addFund(self, inmateNumber, amount) :
		query = '''
			update {tableName}
			set {column_credit} = {column_credit} + %s
			where {column_inmate_number} = %s
		'''.format(
			tableName = self.table,
			column_credit = self.column_credit,
			column_inmate_number = self.column_inmate_number,
		)
		with self.getDatabase().cursor() as cursor :
			cursor.execute(query, [ amount, inmateNumber ])

	def executeBilling(self, inmateNumber, amount, serviceExpiry, billingDay) :
		query = '''
			update {tableName}
			set {column_credit} = {column_credit} - %s,
				{column_service_expiry} = %s,
				{column_billing_day} = %s
			where {column_inmate_number} = %s
		'''.format(
			tableName = self.table,
			column_credit = self.column_credit,
			column_service_expiry = self.column_service_expiry,
			column_billing_day = self.column_billing_day,
			column_inmate_number = self.column_inmate_number,
		)
		with self.getDatabase().cursor() as cursor :
			cursor.execute(query, [
				amount,
				serviceExpiry,
				billingDay,
				inmateNumber
			])

	def setInmateNumberAndPhoneNumber(self, email, inmateNumber, phoneNumber) :
		data = {}
		data[self.column_inmate_number] = inmateNumber
		data[self.column_phone_number] = phoneNumber
		where = {}
		where[self.column_email] = email
		self.update(
			self.table,
			data,
			where
		)

	def setEmailAndPassword(self, inmateNumber, email, password) :
		data = {}
		data[self.column_email] = email
		data[self.column_corrlinks_password] = password
		where = {}
		where[self.column_inmate_number] = inmateNumber
		self.update(
			self.table,
			data,
			where
		)

	def deleteByEmail(self, email) :
		where = {}
		where[self.column_email] = email
		self.delete(
			self.table,
			where
		)

	def setPhoneNumber(self, inmateNumber, phoneNumber = '') :
		data = {}
		data[self.column_phone_number] = phoneNumber
		where = {}
		where[self.column_inmate_number] = inmateNumber
		self.update(
			self.table,
			data,
			where
		)

