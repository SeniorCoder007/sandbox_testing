import actions.service as service
import actions.quit as quit
import actions.test as test

from argparse import ArgumentParser

def doMain() :
	argumentParser = ArgumentParser()
	argumentParser.add_argument('-a', '--action', dest = 'action', default = 'service', help = 'Execute the action')
	args = argumentParser.parse_args()

	action = args.action.lower()
	runner = None

	if action == 'service' :
		runner = service.Service()
	elif action == 'quit' :
		runner = quit.Quit()
	elif action == 'test' :
		runner = test.Test()

	if runner == None :
		print('Unknow action: %s' % (action))
		return

	runner.run()

doMain()
