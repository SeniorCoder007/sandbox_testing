import app.appsession as appsession
import common.util as util

class Quit :
	def run(self) :
		session = appsession.AppSession()
		session.load()
		if not session.isRunning() :
			print("Application is not running.")
			return

		session.setQuit(True)
		session.save()
		
		count = 60
		while session.isRunning() :
			util.sleepForSeconds(1)
			session.load()
			count -= 1
			if count <= 0 :
				break

		if session.isRunning() :	
			print("Warning: application doesn't quit and timeout.")
		else :
			print("Application has quit successfully.")
