import workers.baseworker as baseworker
import models.taskmodel as taskmodel
import models.processedsmsmodel as processedsmsmodel
import app.constants as constants
import app.signalwire as signalwire
import app.appconfig as appconfig
import common.util as util
import common.logger as logger

import datetime

class SmsMonitor(baseworker.BaseWorker) :
	def __init__(self) :
		baseworker.BaseWorker.__init__(self)
		self._fetchDays = 7
		self._processedSmsModel = processedsmsmodel.ProcessedSmsModel()

	def _doThreadRun(self) :
		sw = signalwire.SignalWire.getSignalWire()
		timeAfter = appconfig.AppConfig.getConfig().getSignalWireTimeAfter()
		while not self.shouldStop() :
			fetchDays = self._fetchDays
			self._fetchDays = 1
			since = datetime.date.today() - datetime.timedelta(days = fetchDays)
			messageList = sw.getMessageList(since)
			for message in messageList :
				if self._processedSmsModel.smsWasProcessed(message['uid']) :
					continue
				if message['createTime'] < timeAfter :
					continue
				taskmodel.TaskModel().addTask({
					'type' : constants.TaskType.receivedMessageFromSms,
					'content' : message
				})
				self._processedSmsModel.processedSms(message['uid'])
			self.sleepForSeconds(30)

