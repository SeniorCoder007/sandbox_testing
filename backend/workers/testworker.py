import workers.baseworker as baseworker
import models.taskmodel as taskmodel
import models.processedemailmodel as processedemailmodel
import app.corrlinks as corrlinks
import app.application as application
import app.constants as constants
import app.apputil as apputil
import app.applang as applang
import common.util as util
import common.mailfetcher as mailfetcher
import app.signalwire as signalwire

import datetime
import re

class TestWorker(baseworker.BaseWorker) :
	def __init__(self) :
		baseworker.BaseWorker.__init__(self)
	
	def _doThreadRun(self) :
		corr = corrlinks.CorrLinks('tonysoprano6959+945496082@gmail.com', 'uiuQfCmqmr55')
		print(corr.getInboxItemList(unreadOnly = True))
		return
		params = {
			'inmateNumber' : '12345678',
			'email' : 'aaa@bbb.com',
			'phoneNumber' : '111',
			'amount' : '38'
		}
		apputil.sendMail(
			to = 'wqking@outlook.com',
			subject = applang.AppLang.getLang().getText('message.depositeMailSubject', params),
			message = applang.AppLang.getLang().getText('message.depositeMailBody', params),
		)

	def _testSignalWire(self) :
		#signalwire.SignalWire.getSignalWire().sendMessage('+12029085501', '+12029085500', 'This is a test')
		pass
		
	def _testProcessedEmailModel(self) :
		model = processedemailmodel.ProcessedEmailModel()
		uidList = [ '316', '1', '2' ]
		print(model.getUnprocessedUidList(uidList, 'aaa@bbb.com', 'inbox'))
		model.processedEmail(uidList[0], 'aaa@bbb.com', 'inbox')
		print(model.getUnprocessedUidList(uidList, 'aaa@bbb.com', 'inbox'))
		
	def _testImap(self) :
		mail = mailfetcher.MailFetcher('wqkingtest1@outlook.com', 'Test1978')
		uidList = mail.getUidsSinceDate(datetime.date.today() - datetime.timedelta(1000))
		print(mail.fetchMessageList([ uidList[-1] ])[0])

	def _testTasks(self) :
		'''
		for i in range(10) :
			taskmodel.TaskModel().addTask({
				'type' : constants.TaskType.test1,
				'content' : 'abcdef',
			})
		'''
		while not self.shouldStop() :
			taskmodel.TaskModel().addTask({
				'type' : constants.TaskType.test1,
				'content' : 'abcdef',
			})
			util.sleepForMilliseconds(1000)

	def _testSendNewMessage(self) :
		corr = corrlinks.CorrLinks('tonysoprano6959@gmail.com', '21Corrlinks$')
		corr.sendNewMessage('04068095', 'Ignore this', '''
			This is just another test, ignore it.
		'''
		)

	def _testCorrLinks(self) :
		corr = corrlinks.CorrLinks('tonysoprano6959@gmail.com', '21Corrlinks$')
		print(corr.getInboxItemList(unreadOnly = False))
		return

		#corr.login()
		#corr.register('aaa@bbb.com', 'Qw123456', 'aaa', 'bbb')
	