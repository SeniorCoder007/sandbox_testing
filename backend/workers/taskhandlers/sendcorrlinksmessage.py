import workers.taskhandlers.taskhandler as taskhandler
import models.inmatemodel as inmatemodel
import app.corrlinks as corrlinks
import app.apputil as apputil
import common.util as util
import common.logger as logger

class SendCorrLinksMessage(taskhandler.TaskHandler) :
	def __init__(self) :
		taskhandler.TaskHandler.__init__(self)

	def _doProcess(self) :
		logger.debug("Task SendCorrLinksMessage")
		task = self.getTaskContent()
		inmateNumber = task['inmateNumber']
		subject = task['subject']
		message = task['message']

		inmateModel = inmatemodel.InmateModel()
		inmate = inmateModel.getInmateByInmateNumber(inmateNumber)
		if inmate == None :
			logger.error("SendCorrLinksMessage: not found inmate number %s" % (inmateNumber))
			return taskhandler.HandlerResult.failed

		corr = corrlinks.CorrLinks(inmate[inmateModel.column_email], inmate[inmateModel.column_corrlinks_password])
		if not corr.sendNewMessage(inmate[inmateModel.column_inmate_number], subject, message) :
			return taskhandler.HandlerResult.postpone

		return taskhandler.HandlerResult.success
