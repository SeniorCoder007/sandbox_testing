import workers.taskhandlers.taskhandler as taskhandler
import app.apputil as apputil
import common.util as util
import common.logger as logger

class SendMail(taskhandler.TaskHandler) :
	def __init__(self) :
		taskhandler.TaskHandler.__init__(self)

	def _doProcess(self) :
		logger.debug("Task SendMail")
		task = self.getTaskContent()
		to = util.trim(task['to'])
		if util.isEmptyText(to) :
			return taskhandler.HandlerResult.success
		subject = task['subject']
		message = task['message']
		apputil.sendMail(
			to = to,
			subject = subject,
			message = message
		)
		return taskhandler.HandlerResult.success
