import common.logger as logger
import models.taskmodel as taskmodel
import app.application as application

import enum

@enum.unique
class HandlerResult(enum.IntEnum) :
	success = 0
	postpone = 1
	failed = 2

class TaskHandler :
	def __init__(self) :
		self._task = None

	def initialize(self, task, taskProvider) :
		self._task = task
		self._taskProvider = taskProvider
		self._taskContent = self._task[taskmodel.TaskModel.column_content]
		if self._taskContent == None :
			logger.error("TaskHandler: malformed task content %s" % str(self._task))
			self._taskProvider.completeTask(self._task)

	def getTaskContent(self) :
		return self._taskContent

	def getTaskProvider(self) :
		return self._taskProvider

	def getConfig(self) :
		return application.Application.getApplication().getConfig()

	def process(self) :
		logger.debug("TaskHandler. task type=%s" % (type(self).__name__))
		if self._taskContent == None :
			return
		try :
			result = self._doProcess()
			if result == HandlerResult.success :
				self._taskProvider.completeTask(self._task)
			elif result == HandlerResult.failed :
				self._taskProvider.completeTask(self._task)
			elif result == HandlerResult.postpone :
				self._taskProvider.postponeTask(self._task)
			else :
				if result == None :
					result = -1
				logger.error('TaskHandler: unknow handler result %d' % (int(result)))
		except :
			logger.exception('TaskHandler: exception')
			self._taskProvider.completeTask(self._task)

	def _doProcess(self) :
		pass
