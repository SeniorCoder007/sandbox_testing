import workers.taskhandlers.taskhandler as taskhandler
import app.constants as constants
import app.corrlinks as corrlinks
import app.apputil as apputil
import app.applang as applang
import app.signalwire as signalwire
import models.inmatemodel as inmatemodel
import models.supportmessagemodel as supportmessagemodel
import common.util as util
import common.logger as logger

import re

class ReceivedMessageFromCorrLinks(taskhandler.TaskHandler) :
	def __init__(self) :
		taskhandler.TaskHandler.__init__(self)

	def _doProcess(self) :
		logger.debug("Task ReceivedMessageFromCorrLinks")
		task = self.getTaskContent()
		inmateNumber = apputil.getInmateNumberFromSender(task['sender'])
		if inmateNumber == None :
			logger.error("ReceivedMessageFromCorrLinks: Can't find inmate number in sender: " + task['sender'])
		
		subject = task['subject']
		message = task['message']

		if inmateNumber == None :
			logger.error("ReceivedMessageFromCorrLinks: unknow inmate number")
			return taskhandler.HandlerResult.failed

		inmateModel = inmatemodel.InmateModel()
		inmate = inmateModel.getInmateByInmateNumber(inmateNumber)
		if inmate == None :
			logger.error("ReceivedMessageFromCorrLinks: unregistered inmate number %s" % (inmateNumber))
			return taskhandler.HandlerResult.failed

		fullText = subject + message
		phoneNumber = apputil.extractPhoneNumber(fullText)
		support = apputil.extractSupport(fullText)

		if phoneNumber == None and support == None :
			return self._doHandleUndelivableMessage(
				subject = subject,
				message = message,
				inmateNumber = inmateNumber,
				inmateModel = inmateModel,
				inmate = inmate
			)

		doSupport = False
		if phoneNumber == None :
			doSupport = True
		else :
			if support != None :
				if support['index'] < phoneNumber['index'] :
					doSupport = True

		if doSupport :
			return self._doHandleSupportMessage(
				subject = subject,
				message = message,
				inmateNumber = inmateNumber
			)

		return self._doDeliveryMessage(
			subject = subject,
			message = message,
			phoneNumber = phoneNumber['number'],
			inmateNumber = inmateNumber,
			inmateModel = inmateModel,
			inmate = inmate
		)

	def _doDeliveryMessage(self, subject, message, phoneNumber, inmateNumber, inmateModel, inmate) :
		if not self.getConfig().isTargetPhoneNumberAllowed(phoneNumber) :
			logger.error("ReceivedMessageFromCorrLinks: target phone number %s is disallowed" % (phoneNumber))
			return taskhandler.HandlerResult.success

		if apputil.isServiceDisabled(inmate) :
			return self._doHandleDisabledService(message = message, inmateModel = inmateModel, inmate = inmate)

		maxLength = 1500
		text = message
		text = apputil.removePreviousMessage(text)
		text = apputil.formatSmsMessage(inmate, text)

		messageList = apputil.splitTextAtWordBoundary(text, maxLength)
	
		fromPhoneNumber = inmate[inmateModel.column_phone_number]
		signalWire = signalwire.SignalWire.getSignalWire()
		for m in messageList :
			ok = signalWire.sendMessage(fromPhoneNumber, phoneNumber, m)
			if not ok :
				return taskhandler.HandlerResult.postpone
	
		return taskhandler.HandlerResult.success

	def _doHandleDisabledService(self, message, inmateModel, inmate) :
		corr = corrlinks.CorrLinks(inmate[inmateModel.column_email], inmate[inmateModel.column_corrlinks_password])
		newSubject = applang.AppLang.getLang().getText("message.serviceDisabledSubject")
		newMessage = applang.AppLang.getLang().getText("message.serviceDisabledBody", {
			'message' : message,
			'url' : self.getConfig().getUrlWebsite(),
		})
		corr.sendNewMessage(inmate[inmateModel.column_inmate_number], newSubject, newMessage)
		return taskhandler.HandlerResult.success

	def _doHandleSupportMessage(self, subject, message, inmateNumber) :
		supportMessageModel = supportmessagemodel.SupportMessageModel()
		supportMessageModel.addMessage(inmateNumber, subject, message)

		return taskhandler.HandlerResult.success

	def _doHandleUndelivableMessage(self, subject, message, inmateNumber, inmateModel, inmate) :
		corr = corrlinks.CorrLinks(inmate[inmateModel.column_email], inmate[inmateModel.column_corrlinks_password])
		newSubject = applang.AppLang.getLang().getText("message.unknowMessageSubject")
		newMessage = applang.AppLang.getLang().getText("message.unknowMessageBody", {
			'message' : message
		})
		corr.sendNewMessage(inmate[inmateModel.column_inmate_number], newSubject, newMessage)
		return taskhandler.HandlerResult.success
